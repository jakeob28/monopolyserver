package me.jakeobrien.monopolyserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by Jake on 8/15/2017.
 */
public class SocketServer {
    static HashMap<Integer, ClientHandler> clientHandlers = new HashMap<>();
    public static void startSocketServer(int portNumber) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            Socket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            ClientHandler clientHandler = new ClientHandler(clientSocket);
            clientHandler.start();
            for(int i = 1; i < 6; i++) {
                if (i > 5) {
                    if (!clientHandlers.containsKey(i)) {
                        clientHandlers.put(i, clientHandler);
                        clientHandler.sendMessage("playernumber: " + i);
                        break;
                    }
                } else {
                    clientHandler.sendMessage("error: game full");
                }
            }
        }
    }
    public static void sendAll(String s) {
        for (ClientHandler c : clientHandlers.values()) {
            c.sendMessage(s);
        }
    }
}
