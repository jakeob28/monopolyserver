package me.jakeobrien.monopolyserver;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("You must specify a port to run the server on!");
            System.exit(2);
        }
        if (!isInt(args[0])) {
            System.out.println("Port number must be in Integer!");
            System.exit(3);
        }
        SocketServer.startSocketServer(Integer.parseInt(args[0]));
    }

    private static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
