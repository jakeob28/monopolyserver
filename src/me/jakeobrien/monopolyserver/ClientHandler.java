package me.jakeobrien.monopolyserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;

/**
 * Created by Jake on 7/8/2017.
 */
public class ClientHandler extends Thread {
    private Socket clientSocket;
    public PrintWriter out;
    public BufferedReader in;
    public ClientHandler(Socket client) {
        clientSocket = client;
    }
    @Override
    public void run() {
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine;
            List<String> outputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Recieved " + inputLine);
                outputLine = CommandProtocol.processInput(inputLine);
                if (outputLine != null) {
                    for (String s : outputLine) {
                        out.println(s);
                    }
                    if (outputLine.equals("CLOSE SOCKET") || inputLine.equals("CLOSE SOCKET")) {
                        clientSocket.close();
                        break;
                    }
                }
            }
        } catch (SocketException e) {
            System.out.println("SocketException: Most likely client was shut down");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public void sendMessage(String s) {
        out.println(s);
        out.flush();
    }
}
